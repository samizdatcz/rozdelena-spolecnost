//decka
(function() {
    var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#container_decka').highcharts({

        title: {
            text: 'Americké děti, které vydělávají víc než rodiče'
        },

        subtitle: {
            text: 'Každá další generace má menší šanci překonat příjmy rodičů'
        },

        xAxis: {
            title: {
                text: 'Příjmový percentil rodičů'
            }
        },

        yAxis: {
            title: {
                text: 'Šance vydělávat víc než rodiče (%)'
            },
            plotLines: [{
                color: 'grey',
                value: 50,
                width: 2,
                label: {
                    text: 'průměrná šance překonat rodiče<br>u dětí narozených<br>v roce 1980: 50 %',
                    y: 20,
                }
            }]
        },

		tooltip: {
            formatter: function() {
                return '<b>Narození v ' + this.series.name + ', příjmy rodičů v ' + this.x + '. percentilu:</b> šance vydělávat víc než rodiče je <b>' + this.y + ' %</b>'
            },
            crosshairs: true
        },

        legend: {
        },

        exporting: {
                    enabled: false
        },

        plotOptions: {
            series: {
                    compare: 'value',
                    lineWidth: 2,
                    marker: { symbol: 'circle' }
            }
        },

        credits: {
            href : "http://www.equality-of-opportunity.org/",
            text : "Zdroj: The Equality of Opportunity Project"
        },

        series: [{
            name: '1940',
            data: [
            [1, 95],
            [10, 94],
            [20, 94],
            [30, 93],
            [40, 93],
            [50, 93],
            [60, 94],
            [70, 94],
            [80, 92],
            [90, 88],
            [95, 84],
            [100, 41],
        ],
            color: 'rgba(0,0,0,0.1)',
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: 'rgba(0,0,0,0.1)'
            }
        }, {
            name: '1950',
            data: [
            [1, 91],
            [10, 88],
            [20, 86],
            [30, 84],
            [40, 83],
            [50, 82],
            [60, 79],
            [70, 77],
            [80, 73],
            [90, 65],
            [95, 57],
            [100, 16],
        ],
            color: 'rgba(0,0,0,0.2)',
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: 'rgba(0,0,0,0.2)'
            }
        }, {
            name: '1960',
            data: [
            [1, 87],
            [10, 78],
            [20, 71],
            [30, 67],
            [40, 65],
            [50, 63],
            [60, 62],
            [70, 59],
            [80, 54],
            [90, 45],
            [95, 38],
            [100, 16],
        ],
            color: 'rgba(0,0,0,0.3)',
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: 'rgba(0,0,0,0.3)'
            }
        }, {
            name: '1970',
            data: [
            [1, 90],
            [10, 81],
            [20, 70],
            [30, 65],
            [40, 63],
            [50, 59],
            [60, 57],
            [70, 55],
            [80, 49],
            [90, 44],
            [95, 36],
            [100, 17],
        ],
            color: 'rgba(0,0,0,0.4)',
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: 'rgba(0,0,0,0.4)'
            }
        }, {
            name: '1980',
            data: [
            [1, 80],
            [10, 70],
            [20, 57],
            [30, 50],
            [40, 47],
            [50, 46],
            [60, 43],
            [70, 41],
            [80, 38],
            [90, 33],
            [95, 29],
            [100, 9],
        ],
            color: colors[0],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[0]
            }
        }]
    });
});
})();

//duvera
(function() {
    var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    var demokracie = [
            [1072915200000, 54.1],			//2004
            [1167609600000, 49.2], 			//2007
            [1293840000000, 48.6], 			//2011
            [1356998400000, 44.7], 			//2013
            [1451606400000, 50.6]			//2016
        ],
        demokracie_int = [
            [1072915200000, 51.6, 56.6], 	//2004
            [1167609600000, 46.7, 51.7], 	//2007
            [1293840000000, 46.0, 51.1], 	//2011
            [1356998400000, 42.2, 47.2], 	//2013
            [1451606400000, 48.1, 53.1]		//2016
        ],
        soudrznost = [
            [1104537600000, 21.8],			//2005
            [1325376000000, 14.7], 			//2012
            [1420070400000, 22.8]			//2015
        ],
        soudrznost_int = [
            [1104537600000, 19.7, 23.9], 	//2005
            [1325376000000, 12.9, 16.5], 	//2012
            [1420070400000, 20.7, 25.0]		//2015
        ],
        snemovna = [
            [631152000000, 41.9],           //1990
            [757382400000, 25.0],           //1994
            [883612800000, 22.0],           //1998
            [978307200000, 24.8],			//2001
            [1072915200000, 23.6], 			//2004
            [1199145600000, 22.3], 			//2008
            [1325376000000, 12.9], 			//2012
            [1451606400000, 25.9]			//2016
        ],
        snemovna_int = [
            [631152000000, 39.3, 44.5],     //1990
            [757382400000, 22.8, 27.2],     //1994
            [883612800000, 19.9, 24.1],     //1998
            [978307200000, 22.5, 27.0], 	//2001
            [1072915200000, 21.4, 25.7], 	//2004
            [1199145600000, 20.2, 24.4], 	//2008
            [1325376000000, 11.1, 14.6], 	//2012
            [1451606400000, 23.6, 28.1]		//2016
        ],
        politika = [
            [694224000000, 20.5],           //1992
            [788918400000, 44.1],           //1995
            [883612800000, 18.6],           //1998
            [978307200000, 32.8],			//2001
            [1072915200000, 14.9], 			//2004
            [1199145600000, 12.8], 			//2008
            [1325376000000, 5.4], 			//2012
            [1451606400000, 17.9]			//2016
        ],
        politika_int = [
            [694224000000, 18.4, 22.6],     //1992
            [788918400000, 41.6, 46.6],     //1995
            [883612800000, 16.3, 20.9],     //1998
            [978307200000, 30.4, 35.2], 	//2001
            [1072915200000, 13.1, 16.7], 	//2004
            [1199145600000, 11.1, 14.5], 	//2008
            [1325376000000, 4.3, 6.6], 		//2012
            [1451606400000, 15.9, 19.9]		//2016
        ],
        tisk = [
            [1009843200000, 55.8],			//2002
            [1072915200000, 61.1], 			//2004
            [1199145600000, 57.1], 			//2008
            [1325376000000, 49.6], 			//2012
            [1451606400000, 35.7]			//2016
        ],
        tisk_int = [
            [1009843200000, 53.2, 58.3], 	//2002
            [1072915200000, 58.6, 63.5], 	//2004
            [1199145600000, 54.6, 59.6], 	//2008
            [1325376000000, 47.0, 52.1], 	//2012
            [1451606400000, 33.3, 38.2]		//2016
        ],
        televize = [
            [1009843200000, 62.2],          //2002
            [1072915200000, 63.6],          //2004
            [1199145600000, 67.7],          //2008
            [1325376000000, 56.7],          //2012
            [1451606400000, 41.0]           //2016
        ],
        televize_int = [
            [1009843200000, 59.7, 64.7],    //2002
            [1072915200000, 61.2, 66.1],    //2004
            [1199145600000, 65.3, 70.1],    //2008
            [1325376000000, 54.2, 59.2],    //2012
            [1451606400000, 38.5, 43.5]     //2016
        ];

    $('#container_duvera').highcharts({

        title: {
            text: 'Nespokojenost v české společnosti'
        },

        xAxis: {
            type: 'datetime',
            tickInterval: 31536000000 // 1 rok
        },

        yAxis: {
            title: {
                text: null
            },
            min: 0
        },

		tooltip: {
            formatter: function() {
                switch(this.series.name) {
                 case 'důvěra v demokracii': return '<br>Pro <b>' + this.y + ' %</b> respondentů je demokracie je lepší než jakýkoliv jiný způsob vlády.'; break;
                 case 'soudržnost společnosti': return 'Podle <b>' + this.y + ' %</b> respondentů v naší společnosti převažuje atmosféra soudržnosti, důvěry a solidarity.'; break;
                 case 'důvěra poslanecké sněmovně': return '<b>' + this.y + ' %</b> respondentů důvěřuje Poslanecké sněmovně PČR.'; break;
                 case 'spokojenost s politikou': return '<b>' + this.y + ' %</b> respondentů je spokojeno s českou politikou.'; break;
                 case 'důvěra tisku': return '<b>' + this.y + ' %</b> respondentů důvěřuje tisku.'; break;
                 case 'důvěra televizi': return '<b>' + this.y + ' %</b> respondentů důvěřuje televizi.'; break;
                }
            },
            crosshairs: true
        },

        legend: {
        },

        exporting: {
                    enabled: false
        },

        plotOptions: {
            series: {
                    compare: 'value',
                    lineWidth: 2,
                    marker: { symbol: 'circle' }
            }
        },

        credits: {
            href : "http://nesstar.soc.cas.cz/",
            text : "Zdroj: CVVM"
        },

        series: [{
            name: 'důvěra v demokracii',
            data: demokracie,
            zIndex: 1,
            color: colors[0],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[0]
            }
        }, {
            name: 'důvěra v demokracii',
            data: demokracie_int,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            legend: false,
            enableMouseTracking: false,
            color: colors[0],
            fillOpacity: 0.2,
            zIndex: 0
        }, {
            name: 'soudržnost společnosti',
            data: soudrznost,
            zIndex: 1,
            color: colors[1],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[1]
            }
        }, {
            name: 'soudržnost společnosti',
            data: soudrznost_int,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            legend: false,
            enableMouseTracking: false,
            color: colors[1],
            fillOpacity: 0.2,
            zIndex: 0
        }, {
            name: 'důvěra poslanecké sněmovně',
            data: snemovna,
            zIndex: 1,
            color: colors[2],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[2]
            }
        }, {
            name: 'důvěra poslanecké sněmovně',
            data: snemovna_int,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            legend: false,
            enableMouseTracking: false,
            color: colors[2],
            fillOpacity: 0.2,
            zIndex: 0
        }, {
            name: 'spokojenost s politikou',
            data: politika,
            zIndex: 1,
            color: colors[3],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[3]
            }
        }, {
            name: 'spokojenost s politikou',
            data: politika_int,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            legend: false,
            enableMouseTracking: false,
            color: colors[3],
            fillOpacity: 0.2,
            zIndex: 0
        }, {
            name: 'důvěra tisku',
            data: tisk,
            zIndex: 1,
            color: colors[4],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[4]
            }
        }, {
            name: 'důvěra tisku',
            data: tisk_int,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            legend: false,
            enableMouseTracking: false,
            color: colors[4],
            fillOpacity: 0.2,
            zIndex: 0
        }, {
            name: 'důvěra televizi',
            data: televize,
            zIndex: 1,
            color: colors[5],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[5]
            }
        }, {
            name: 'důvěra televizi',
            data: televize_int,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            legend: false,
            enableMouseTracking: false,
            color: colors[5],
            fillOpacity: 0.2,
            zIndex: 0
        }]
    });
});
})();

//duveravek
(function() {
     var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#container_duveravek').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Pro koho je demokracie lepší než autoritativní režim (Česko)?'
        },
        subtitle: {
            text: 'Podíl lidí, pro které je demokratický režim lepší než autoritativní, podle roku narození'
        },
        xAxis: {
            categories: ['30s', '40s', '50s', '60s', '70s', '80s'],
            title: {
                text: 'Dekáda narození'
            }
        },
        yAxis: {
            title: {
                text: 'Podpora demokracie (%)'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + ', narození v ' + this.x + ':</b> demokratický režim je nezbytný pro <b> ' + this.y + ' %</b>'
            },
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://nesstar.soc.cas.cz/',
            text : 'Zdroj: CVVM, únor 2016'
        },
        plotOptions: {
        },
        series: [{
            name: 'Česko',
            data: [42.4, 41.7, 50.0, 58.4, 54.5, 56.2],
            color: colors[0]
        }]
    });
});
})();

//duveraveksvet
(function() {
    var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#container_duveraveksvet').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Kdo považuje demokracii za nezbytnou (ve světě)?'
        },
        subtitle: {
            text: 'Podíl lidí, pro které je demokratický režim nezbytný, podle roku narození'
        },
        xAxis: {
            categories: ['30s', '40s', '50s', '60s', '70s', '80s'],
            title: {
                text: 'Dekáda narození'
            }
        },
        yAxis: {
            title: {
                text: 'Podpora demokracie (%)'
            }
        },
        tooltip: {
            valueSuffix: ' %'
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://www.journalofdemocracy.org/sites/default/files/Foa%26Mounk%20-%20JoD%2028.1%20-%20PRE-PRINT%20VERSION.pdf',
            text : 'Zdroj: Foa, Mounk: The Signs of Deconsolidation'
        },
        plotOptions: {
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + ', narození v ' + this.x + ':</b> demokratický režim je nezbytný pro <b> ' + this.y + ' %</b>'
            },
            crosshairs: true
        },
        series: [{
            name: 'Švédsko',
            data: [82.9, 81.2, 84.2, 78.8, 72.5, 59.2],
            color: colors[0],
            visible: false
        }, {
            name: 'Austrálie',
            data: [77.5, 75, 73.3, 62.1, 50.4, 40.4],
            color: colors[1],
            visible: false
        }, {
            name: 'Nizozemsko',
            data: [52.1, 51.7, 50.8, 46.3, 42.5, 35],
            color: colors[2],
            visible: false
        }, {
            name: 'Spojené státy',
            data: [73.3, 62.1, 57.5, 49.6, 42.5, 30.8],
            color: colors[3]
        }, {
            name: 'Nový Zéland',
            data: [70, 64.6, 61.7, 52.9, 50, 29.6],
            color: colors[4],
            visible: false
        }, {
            name: 'Velká Británie',
            data: [70.4, 58.8, 62.1, 63.3, 47.9, 29.2],
            color: colors[5],
            visible: false
        }]
    });
});
})();

//prezident
(function() {
   var colors = ['rgba(255,0,0,0.5)', 'rgba(128,0,128,0.5)', 'rgb(255,0,0)', 'rgb(128,0,128)']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#container_prezident').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Druhé kolo prezidentské volby'
        },
        subtitle: {
            text: 'Odpovědi na otázku <i>koho jste volili v druhém kole prezidentské volby?</i>'
        },
        xAxis: {
            categories: ['věřící', 'celá populace', 'muži', 'města', 'vyšší příjem', 'Praha', 'vysokoškolský titul', 'nižší věk']
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'podíl voličů v dané skupině (%)'
            },
            plotLines: [{
                color: 'grey',
                dashStyle: 'dash',
                value: 58.8,
                width: 1,
                label: {
                    x: -10,
                    text: 'Miloš Zeman',
                    align: 'right',
                    rotation: 0
                }
            }, {
                color: 'grey',
                dashStyle: 'dash',
                value: 58.8,
                width: 1,
                label: {
                    x: 10,
                    text: 'Karel Schwarzenberg',
                    align: 'left',
                    rotation: 0
                }
            }]
        },
        tooltip: {
            formatter: function() {
                switch(this.x) {
                    case 'věřící': return 'Kdyby volili pouze <b>věřící</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'celá populace': return 'Mezi <b>všemi respondenty</b> měl ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'muži': return 'Kdyby volili pouze <b>muži</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'města': return 'Kdyby volili pouze <b>obyvatelé měst nad 80 tisíc obyvatel</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'vyšší příjem': return 'Kdyby volili pouze <b>lidé s čistým příjmem nad 14 tisíc korun</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'Praha': return 'Kdyby volili pouze <b>Pražané</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'vysokoškolský titul': return 'Kdyby volili pouze <b>lidé s vysokoškolským titulem</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                    case 'nižší věk': return 'Kdyby volili pouze <b>lidé pod 45 let</b>, měl by ' + this.series.name + ' podporu <b>' + this.y + ' %</b>'; break;
                }
            },
            crosshairs: true
        },
        legend: {
            reversed: true
        },
        exporting: {
                    enabled: false
        },
        credits: {
            href : "http://nesstar.soc.cas.cz/",
            text : "Zdroj: CVVM, únor 2013"
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Karel Schwarzenberg',
            data: [{y: 38.0}, {y: 41.2, color: colors[3]}, {y: 42.2}, {y: 45.8}, {y: 51.9}, {y: 53.6}, {y: 56.6}, {y: 69.9}],
            color: colors[1]
        }, {
            name: 'Miloš Zeman',
            data: [{y: 62.0}, {y: 58.8, color: colors[2]}, {y: 57.8}, {y: 54.2}, {y: 48.1}, {y: 46.4}, {y: 43.4}, {y: 30.1}],
            color: colors[0]
        }]
    });
});
})();

//prijmy
(function() {
   var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#container_prijmy').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Kdo si polepšil?'
        },
        subtitle: {
            text: 'Změna rodinného příjmu mezi lety 2003 a 2013 podle příjmové skupiny'
        },
        xAxis: {
            categories: ['nejchudší', '2. decil', '3. decil', '4. decil', '5. decil', '6. decil', '7. decil', '8. decil', '9. decil', 'nejbohatší'],
        },
        yAxis: {
            title: {
                text: '%'
            }
        },
        tooltip: {
            valueSuffix: ' %'
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'https://www.czso.cz/csu/czso/archiv-vybranych-publikaci-prijmy-vydaje-a-zivotni-podminky-domacnosti',
            text : 'Zdroj: ČSÚ'
        },
        plotOptions: {
        },
        tooltip: {
            valueSuffix: ' %'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'nárůst reálného příjmu mezi lety 2003 a 2013',
            data: [7.6, 17.6, 19.0, 20.9, 20.9, 20.1, 18.8, 18.7, 19.0, 31.6],
            color: colors[3]
        }]
    });
});
})();

//slon
(function() {
    var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    var data = [
            [5, -1.1],
            [10, 43.9],
            [15, 55.9],
            [20, 63.7],
            [25, 67.7],
            [30, 67],
            [35, 70.1],
            [40, 73.5],
            [45, 71.7],
            [50, 80],
            [55, 64.0],
            [60, 71.1],
            [65, 61.2],
            [70, 38.7],
            [75, 9.4],
            [80, -5.4],
            [85, -2],
            [90, 6.6],
            [95, 17.4],
            [99, 26.7],
            [100, 60],
        ];

    $('#container_slon').highcharts({

        title: {
            text: 'Komu pomohla globalizace?'
        },

        subtitle: {
            text: 'Změna v reálném příjmu mezi roky 1988 a 2008 podle příjmové kategorie (celosvětově)'
        },

        xAxis: {
            title: {
                text: null
            },
            plotBands: [{
                from: 45,
                to: 65,
                color: 'rgba(0,0,0,0.1)',
                borderWidth: 2,
                borderColor: 'white',
                label: {
                    text: 'asijská střední třída',
                }
            }, {
                from: 75,
                to: 90,
                color: 'rgba(0,0,0,0.1)',
                borderWidth: 2,
                borderColor: 'white',
                label: {
                    text: 'západní nižší střední třída'
                }
            }]
        },

        yAxis: {
            title: {
                text: 'Reálný růst příjmu'
            },
            plotLines: [{
                color: 'grey',
                value: 0,
                width: 2
            }]
        },

		tooltip: {
            formatter: function() {
                return '<b>' + this.x + '. percentil:</b> reálný příjem se změnil o <b>' + this.y + ' %</b>'
            },
            crosshairs: true
        },

        legend: {
        },

        exporting: {
                    enabled: false
        },

        plotOptions: {
            series: {
                    compare: 'value',
                    lineWidth: 2,
                    marker: { symbol: 'circle' }
            }
        },

        credits: {
            href : "http://documents.worldbank.org/curated/en/959251468176687085/pdf/wps6259.pdf",
            text : "Zdroj: Branko Milanović, Global Income Inequality by the Numbers"
        },

        series: [{
            name: 'Percentil příjmu (celosvětově)',
            data: data,
            color: colors[0],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[0]
            }
        }]
    });
});
})();

//vzdelani
(function() {
    var colors = ['rgb(255, 65, 54)', 'rgb(0, 116, 217', 'rgba(255, 65, 54, 0.5)', 'rgba(0, 116, 217, 0.5)']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#container_vzdelani').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Vysokoškolské vzdělání'
        },
        subtitle: {
            text: 'Podíl VŠ vzdělaných ve společnosti'
        },
        xAxis: {
            categories: ['Kanada', 'USA', 'UK', 'Finsko', 'Norsko', 'Švédsko', 'Estonsko', 'Španělsko', 'Průměr OECD', 'Francie', 'Polsko', 'Německo', 'Maďarsko', 'Česko', 'Slovensko', 'Itálie']
        },
        yAxis: {
            title: {
                text: '%'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : "https://data.oecd.org/eduatt/adult-education-level.htm",
            text : "Zdroj: OECD"
        },
        plotOptions: {
            bar: {
                stacking: 'normal'
            }
        },
        tooltip: {
            valueSuffix: ' %'
        },
        legend: {
            reversed: true
        },
        series: [{
            name: 'noví absolventi VŠ po 1997',
            data: [{y: 15.1}, {y: 10.5}, {y: 21.1}, {y: 10.1}, {y: 14.3}, {y: 9.7}, {y: 9.1}, {y: 12.5}, {y: 15.7}, {y: 13.5}, {y: 17.5}, {y: 5.0}, {y: 12.0}, {y: 11.6, color: colors[0]}, {y: 10.6}, {y: 8.9}],
            color: colors[2]
        }, {
            name: 'podíl absolventů VŠ v roce 1997',
            data: [{y: 40.1}, {y: 34.1}, {y: 22.6}, {y: 32.6}, {y: 28.4}, {y: 30.1}, {y: 28.9}, {y: 22.6}, {y: 19.3}, {y: 20.0}, {y: 10.2}, {y: 22.6}, {y: 12.2}, {y: 10.6, color: colors[1]}, {y: 10.5}, {y: 8.6}],
            color: colors[3]
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 300
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    });
});
})();