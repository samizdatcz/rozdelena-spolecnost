---
title: "Rozdělená společnost? Čechy proti sobě staví věk a vzdělání"
perex: "Loňskou volbu amerického prezidenta i britské referendum o odchodu z Evropské unie ovládli nespokojení voliči, kteří věří na jednoduchá řešení. Překvapení analytici od té doby pátrají, kde se v takovém množství vzali a co chtějí. Bude se totéž opakovat v Česku?"
description: "Loňskou volbu amerického prezidenta i britské referendum o odchodu z Unie ovládli nespokojení voliči, kteří věří na jednoduchá řešení. Překvapení analytici od té doby pátrají, kde se v takovém množství vzali a co chtějí. Bude se totéž opakovat v Česku?"
authors: ["Jan Boček", "Jaromír Mazák", "Michal Zlatkovský"]
published: "4. ledna 2017"
coverimg: https://interaktivni.rozhlas.cz/data/rozdelena-spolecnost/www/media/cover.jpg
coverimg_note: "Nevrlý starý muž. Foto <a href='https://www.flickr.com/photos/gato-gato-gato/16389415255/in/photostream/'>gato-gato-gato (CC BY-NC-ND 2.0)</a>"
url: "rozdelena-spolecnost"
libraries: [jquery, "https://code.highcharts.com/highcharts.js", "https://code.highcharts.com/highcharts-more.js"]
recommended:
  - link: https://interaktivni.rozhlas.cz/brexit/
    title: Všechno, co jste chtěli vědět o Brexitu
    perex: 23 otázek a odpovědí k britskému referendu o vystoupení z Evropské unie
    image: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
  - link: https://interaktivni.rozhlas.cz/usvolby-explainer/
    title: Sedm momentů, které dovedly Ameriku k Trumpovi
    perex: Jak a proč se vyvíjela americká volební mapa?
    image: https://interaktivni.rozhlas.cz/usvolby-explainer/media/washington.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/rusove-proti-rusum-zeme-v-nevyhlasene-obcanske-valce--1484099
    title: Rusové proti Rusům: Země v nevyhlášené občanské válce
    perex: Rusko prošlo v devadesátých letech demografickou krizí srovnatelnou s válečným konfliktem.
    image: http://media.rozhlas.cz/_obrazek/3372356.jpeg
---

Obě loňské velké volby – zvolení Donalda Trumpa americkým prezidentem i britský odchod z Unie – jako by definitivně pohřbily důvěru většinové společnosti ve schopnost médií a jimi citovaných expertů cokoli vysvětlit či předpovědět.

Přesto se řada analytiků dál snaží co nejlépe pochopit a popsat, co se vlastně stalo. Opravdu se západní společnost rozpadá na dvě či více frakcí, které stále obtížněji nacházejí společnou řeč? A jak z toho ven?

Jedna perspektiva ukazuje, že Donalda Trumpa i brexit zvolili frustrovaní příslušníci střední třídy, na které globální ekonomika zapomněla a plody hospodářského růstu za ně sklízí někdo jiný. Z jiného úhlu pohledu jsou to spíše zastánci konzervativních hodnot, kteří odmítají nadále věřit jakýmkoli „oficiálním autoritám“. Současný svět podle nich nejlépe vystihuje heslo *líp už bylo*.

To jsou ve zkratce závěry anglosaských sociologů, demografů a ekonomů. Dosud ale chybí ucelený pohled na to, zda se stejné propasti mezi společenskými skupinami prohlubují i v Česku. Některé trendy, jako chudnutí střední třídy a bohatnutí nejbohatších, jsou univerzální a Češi v nich kopírují vývoj na západě. V jiných – třeba v neochabující víře mladé generace v demokracii – se globálním trendům vymykají, snad i díky vlastní nedávné zkušenosti s totalitou.

> Česká společnost se potýká s podobnými problémy jako ta americká. Podobají se ale voliči Donalda Trumpa voličům Miloše Zemana?

V následujících kapitolách shrnujeme jednotlivé pohledy na to, jestli a jak příkopy ve společnosti vznikají – od ekonomického a demografického přes zkoumání hodnot, jaké oba společenské tábory vyznávají, po zjišťování, jak velkou roli hraje v prohlubování příkopů volební systém nebo univerzity.

Ukážeme také, jak společnost reaguje na média v rukou nejbohatších Čechů i jak ji dělí vyostřený spor mezi Hradem a kavárnami v podhradí. Nabízíme tak možnou odpověď na otázku, jak je česká společnost polarizovaná a zda hrozí, že propasti znemožní sdílenou představu budoucnosti také u nás.

<ul class="obsah">
  <li><a href="#prijmy">Když bohatí bohatnou a chudí chudnou</a></li>
  <li><a href="#americkysen">Jak se Američanům vzdálil americký sen</a></li>
  <li><a href="#demografie">Stejní a přece jiní: pohled na voliče Trumpa a Zemana</li>
  <li><a href="#vzdelani">Kde se berou liberálové – a proč ne na univerzitách</a></li>
  <li><a href="#polarizace">Česko 2017: Čeká nás politická krize?</a></li>
  <li><a href="#svet"><i>Dekonsolidující demokracie</i>. Je čas ke znepokojení</a></li>
</ul>

## Když bohatí bohatnou a chudí chudnou <a name="prijmy"></a>

V Česku se rozšiřuje příjmová propast mezi nejbohatšími a nejchudšími. Jen mezi lety 2003 s 2013, kdy jsou k dispozici detailní data, se reálný příjem nejbohatší desetiny českých rodin o třetinu zvýšil, zatímco u rodin s nízkými příjmy rostla životní úroveň pomaleji.

„Chudnoucí a slábnoucí střední třída a bohatnoucí bohatí je trend, který v řadě západních ekonomik pozorujeme nejméně od sedmdesátých let,“ vysvětluje sociolog Jakub Macek z brněnské Masarykovy univerzity. „Poválečný rozvoj a vznik sociálního státu střední třídu na čas posílil, ale od recese v polovině sedmdesátých let se trend v řadě zemí obrátil.“

*Oprava: Kvůli chybě v datech jsou skutečné rozdíly mezi příjmovými kategoriemi v Česku menší než v původní verzi grafu.*

<aside class="big">
  <div id="container_prijmy" style="height: 600px;"></div>
</aside>

<div data-bso=1></div>

> Ve studii, jak si mezi lety 1988 a 2008 polepšily příjmové skupiny napříč planetou, dopadla západní střední třída nejhůř.

Podle dat o příjmech rodin tedy Česko patří k zemím, kde se – s výjimkou obou krajních kategorií – příjmová propast v posledních letech příliš neprohlubovala. V tom se česká společnost do značné míry od té americké nebo britské liší.

Srboamerický ekonom Branko Milanović ve statistice, přezdívané podle tvaru křivky *sloní graf*, ukázal „zapomenutou“ vrstvu západních ekonomik: dělníky, prodavačky, v zásadě všechny zaměstnance se špatným vzděláním. Ve [studii z roku 2012](http://documents.worldbank.org/curated/en/959251468176687085/pdf/wps6259.pdf) Milanović zmapoval, jak si mezi lety 1988 a 2008 polepšily příjmové skupiny napříč planetou; západní střední třída dopadla nejhůř.

<aside class="big">
  <div id="container_slon" style="height: 600px;"></div>
</aside>

Střední část grafu ukazuje, jak globalizace pomohla k růstu životní úrovně třetího světa. Vůbec nejvíc z globalizované ekonomiky dokázala vytěžit střední třída v asijských zemích s příjmy někde mezi 50. a 70. percentilem.

Na bohatém západě si výrazně polepšilo pouze jedno procento nejbohatších – špička chobotu. Většina ostatních stagnovala. Obloukem se ekonomický růst vyhnul právě západní střední třídě, která přibližně odpovídá propasti mezi 80. až 90. percentilem. Hůře placená část se rovnou propadla pod životní úroveň z konce osmdesátých let.

> Český scénář se v budoucnosti může podobat tomu americkému.

„[Globální] systém funguje tak, jak bylo inzerováno: mezi rokem 1970 a americkou finanční krizí v roce 2008 se celosvětová produkce zboží a služeb zvedla čtyřnásobně,“ [počítá deník Financial Times](https://www.ft.com/content/6a43cf54-a75d-11e6-8b69-02899e8bd9d1). „Pomohl z chudoby stovkám milionů lidí nejen v Číně a Indii, ale také v Latinské Americe a subsaharské Africe.“

„Jenže jak si dnes všichni bolestně uvědomují, plody tohoto systému se nedostaly ke všem,“ pokračují Financial Times. „Pracující třída v rozvinutém světě o práci přicházela, když firmy v reakci na globální konkurenční boj přesunovaly práci do jiných zemí nebo snižovaly náklady.“

Jak [ukazují výzkumníci z Pew Research Center](http://www.pewsocialtrends.org/2016/05/11/americas-shrinking-middle-class-a-close-look-at-changes-within-metropolitan-areas/), jen v posledních patnácti letech se americká střední třída ztenčila z 55 procent populace na 51 procent. V některých metropolitních oblastech byl propad výrazně vyšší. Jejich reálný příjem je navíc o šest procent nižší než v roce 2000. Ve zkratce: americká ekonomika se dosud nevzpamatovala z finanční krize po roce 2007.

<figure>
<img src="media/chudoba.jpg" width="100%" alt="Žebrající muž v Praze" title="Žebrající muž v Praze">
<figcaption>
<small><i>
Žebrající muž v Praze. Ilustrační foto: <a href="https://commons.wikimedia.org/wiki/Category:Beggars_in_Prague#/media/File:Praha_-_panoramio_-_jan.kolecek.jpg">jan.kolecek/Wikimedia Commons (CC BY 3.0)</a>
</i></small>
</figcaption>
</figure>

Česko a ostatní postkomunistické země v devadesátých letech patřily k těm, kdo po rozpadu komunistického průmyslu brali práci západní střední třídě. Ostatně přesun montoven na východ za levnější pracovní silou vedl k tomu, že dnes Češi a Slováci mají celosvětově nejvíc vyrobených aut na obyvatele. Právě vysoký podíl nekvalifikovaných zaměstnanců znamená, že se český scénář v budoucnosti může podobat tomu americkému.

Nástup populistů v posledních letech totiž živí právě frustrace pracující střední třídy – západní, ale i české. Podpora této části společnosti stála v červnu za hlasováním o vystoupení Velké Británie z Evropské unie, v listopadu přinesla vítězství v prezidentské volbě Donaldu Trumpovi a jen těsně nestačila rakouskému prezidentskému kandidátovi Norbertu Hoferovi. Koneckonců ke stejné části společnosti mluví i český prezident Miloš Zeman.



## Jak se Američanům vzdálil americký sen <a name="americkysen"></a>

V mnoha ekonomicky vyspělých zemích je situace tíživá: špatně placení zaměstnanci mají jen malou naději na životní zlepšení. Do jakých ekonomických podmínek se narodili, v takových s vysokou pravděpodobností stráví zbytek života. O budoucnosti potomků stále častěji rozhoduje, zda jim rodiče dokážou zaplatit kvalitní vzdělání a zajistit dobré startovní podmínky.

Zajímavý posun ve Spojených státech dokumentuje čerstvá studie týmu ekonomů, kterou [převzal web The New York Times](http://www.nytimes.com/2016/12/08/opinion/the-american-dream-quantified-at-last.html). Studie ukazuje, jak u poválečných generací klesá naděje vydělávat víc než rodiče. Američané narození ve čtyřicátých letech podle ní měli víc než devadesátiprocentní šanci překonat příjmy svých rodičů. Oproti tomu u generace narozené v osmdesátých letech je šance na vyšší příjem, než měli rodiče, pouze padesátiprocentní. Americký sen, tedy schopnost zbohatnout vlastními silami, je každé další generaci vzdálenější.

<figure>
<img src="media/americky_sen.jpg" width="100%" alt="Kam zmizel americký sen?" title="Kam zmizel americký sen?">
<figcaption>
<small><i>
Kam zmizel americký sen? Ilustrační foto: <a href="https://www.flickr.com/photos/pennuja/15731623435/">Jim Pennucci/Flickr (CC BY 2.0)</a>
</i></small>
</figcaption>
</figure>

„V osmdesátých letech se vlivem globalizace, technologických změn, vládních reforem, které šly na ruku bohatým, a zpomalení rozvoje vzdělání začala zvyšovat ekonomická nerovnost,“ píšou The New York Times. „Výsledkem byl posun příjmů střední třídy směrem k chudým. Technologický boom devadesátých let zvyšování nerovnosti zpomalil – jenže jen dočasně.“

„Výsledek studie je hluboce alarmující,“ dodává deník. „Je to portrét ekonomiky, která dokáže zklamat obrovské množství lidí. Slyšeli, že se v jejich zemi žije stále lépe. Jenže jejich zkušenost je jiná.“

„Psychologické studie ukazují, že pocit štěstí se odvíjí od srovnávání své životní situace s okolím,“ uzavírá The New York Times. „A je těžké si představit zásadnější srovnání než se svými rodiči.“

<aside class="big">
  <div id="container_decka" style="height: 600px;"></div>
</aside>

Americký sen stále můžete prožít, téměř ve všech vyspělých zemích je to ovšem snazší než ve Spojených státech. [Studie OECD z roku 2010](http://search.oecd.org/eco/labour/49849281.pdf) ukazuje, že šance dosáhnout lepšího vzdělání a příjmu než rodiče – obě charakteristiky spolu úzce souvisí – se ve vyspělých ekonomikách zemi od země drasticky liší.

Zvlášť nízkou *mezigenerační sociální mobilitu*, jak se tahle vlastnost nazývá, mají Velká Británie, Itálie a Spojené státy. Tedy právě ty země, kde se populistům v posledním roce nadmíru dařilo. (V Itálii opozice neustále hrozí referendem o členství v Evropské unii, preference ukazují na podobné rozložení sil jako v britském hlasování.)

Vliv sociální mobility je ostatně dobře patrný na mapě Trumpova volebního úspěchu. „Všimněte si, že Donald Trump výrazněji navyšoval zisk republikánů v amerických státech s nižší ekonomickou mobilitou,“ upozorňují Ondřej Císař a Daniel Prokop z Fakulty sociálních věd Univerzity Karlovy.

Vysoká sociální mobilita je naopak podle čísel OECD ve Skandinávii. Americký sen tedy dnes žijí nejspíš Norové, Finové nebo Dánové. Mimo Evropu se mu daří také v Austrálii a Kanadě.

> Americký sen stále můžete prožít - téměř ve všech vyspělých zemích je to ovšem snazší než ve Spojených státech.

Česká společnost se podle obou vědců potýká s podobným problémem jako ta americká: mezigenerační mobilita klesá. „I naši společnost charakterizuje vysoká mezigenerační reprodukce vzdělání, které přitom silně podmiňuje to, zda jedinec v dospělosti upadne do chudoby nebo se jí dostane na dohled. V České republice se kvůli vzdělávacímu systému ohrožení chudobou v části populace prostě dědí. A přestože u nás není tak častá extrémní chudoba, velká část populace je jí neustále nablízku. Typicky jsou to nezaměstnaní, senioři, ale také rodiny s dětmi nebo pracující v některých profesích.“

První pohled na zastánce jednoduchých řešení tedy naznačuje, že jsou pro ně typické příjmy na hraně přežití – a hlavně malá naděje na zlepšení.

Co dalšího o nich můžeme zjistit? Podobají se voliči Donalda Trumpa nebo britští příznivci vystoupení z unie voličům Miloše Zemana?

## Stejní a přece jiní: Pohled na voliče Trumpa a Zemana <a name="demografie"></a>

Americké prezidentské volby i britské referendum o vystoupení z unie rozdělují společnost na dva tábory: voliče Donalda Trumpa a voliče Hillary Clintonové, respektive příznivce setrvání v evropském spolku a zastánce vystoupení. Otázkou je, čím se kromě podpory jednomu z táborů obě skupiny liší. Je to příjem, pohlaví, věk, vzdělání, víra, život na venkově nebo ve městě či ještě něco dalšího?

Snadnou odpověď hledá řada článků, kterými se překvapení analytici pokusili krátce po volbách vysvětlit Trumpovo vítězství. Shodují se na tom, že klíčovým ukazatelem bylo v letošní volbě vzdělání. „V roce brexitu a Trumpa se stalo vzdělání vůbec největší propastí, která rozděluje společnost na dva čím dál víc nepřátelské tábory,“ [shrnul výsledky většiny analýz britský Guardian](https://www.theguardian.com/politics/2016/oct/05/trump-brexit-education-gap-tearing-politics-apart).

> Prezidentské volby rozdělily americkou společnost na v průměru mladší a vzdělanější obyvatele měst a starší, méně vzdělané Američany z venkova.

V závislosti na metodě, kterou výzkumníci zvolili, bylo vzdělání buď tím vůbec nejsilnějším faktorem určujícím výsledek volby pro Trumpa nebo Clintonovou, nebo součástí širšího spektra faktorů včetně rasy, věku a pohlaví ([analýza Financial Times](https://www.ft.com/content/9fc71e40-b015-11e6-a37c-f4a01f1b0fa1), proti ní [exit poll agentury Pew Research](http://www.pewresearch.org/fact-tank/2016/11/09/behind-trumps-victory-divisions-by-race-gender-education/)). [The Washington Post zase připomněl](https://www.washingtonpost.com/graphics/politics/2016-election/nc-precincts/) rozdíl v podpoře obou kandidátů ve městech a na venkově. Státy, které mají na běžné volební mapě modrou nebo rudou barvu, mohou mít při detailnějším přiblížení mnohem složitější voličské vzorce.

Naopak průměrný příjem se mezi oběma tábory příliš nelišil. To je podle analytiků způsobeno tím, že Hillary Clintonovou volila vedle bohatších a vzdělanějších Američanů také většina Afroameričanů, kteří mají průměrně nižší příjem.

Přestože každá z analýz dává mírně odlišné výsledky, ve zkratce mluví podobně: prezidentské volby rozdělily americkou společnost na v průměru mladší a vzdělanější obyvatele měst a starší, méně vzdělané Američany z venkova. „Černí se bouří, muslimové odpalují bomby, homosexuálové šíří AIDS, mexické kartely popravují děti, ateisté strhávají vánoční stromky,“ [ukazuje rozdílný pohled obou táborů šéfredaktor webu Cracked](http://www.cracked.com/blog/6-reasons-trumps-rise-that-no-one-talks-about/), který se z trumpovského venkova přesunul do clintonovského New Yorku.

<figure>
<img src="media/trump_volic.jpg" width="100%" alt="Tichá většina stojí za Trumpem?" title="Tichá většina stojí za Trumpem?">
<figcaption>
<small><i>
Tichá většina stojí za Trumpem? Ilustrační foto: <a href="https://commons.wikimedia.org/wiki/File:Donald_Trump_supporter_(27761513565).jpg">Gage Skidmore/Wikimedia Commons (CC BY-SA 2.0)</a>
</i></small>
</figcaption>
</figure>

Podobné demografické vzorce jako ve Spojených státech rozhodovaly o necelý půlrok dříve ve Spojeném království. Nejlepším prediktorem volby pro nebo proti brexitu byl podle [analýzy volebních výsledků Guardianu](https://www.theguardian.com/news/datablog/2016/jun/24/the-areas-and-demographics-where-the-brexit-vote-was-won) opět vysokoškolský titul. Dobře dokázala výsledky hlasování předpovědět také společenská třída, kterou britští statistici sledují, a na rozdíl od Spojených států i příjem. Překvapivě nespolehlivým ukazatelem byl věk – očekávaná bitva mladších Britů proti starším se podle Guardianu konala jen v omezené míře.

[The Washington Post připomíná](https://www.washingtonpost.com/news/worldviews/wp/2016/11/27/the-urban-rural-divide-isnt-just-evident-in-american-politics-its-prevalent-in-europe-too/), že stejně jako ve Spojených státech zde hraje roli propast mezi městem a venkovem. Výjimkou je proevropské Skotsko.

Komu by obě země přišly příliš vzdálené, může se podívat na data z prosincového druhého kola prezidentské volby v sousedním Rakousku. Vzdělání je opět dobrým ukazatelem, kdo ve volebním okrsku zvítězil, [jak ukazuje na Twitteru analytik Financial Times](https://twitter.com/jburnmurdoch/status/805711404670058496).

V Česku se v několika posledních letech žádné zásadní referendum nekonalo. Proporční volební systém zase omezuje rozhodnutí buď–anebo na minimum, takže nezbývá mnoho voleb, které by českou společnost rozdělily na dvě části podobně jako Američany prezidentská volba nebo Brity loňské referendum.

<blockquote>Předpovědět jen na základě velikosti obce, kde žijete, zda budete volit toho nebo onoho kandidáta, je v Česku prakticky nemožné.<br>
<span class="citautor">Jakub Macek, sociolog</blockquote>

Nejpříhodnějším momentem pro sledování názorového štěpení české společnosti je druhé kolo prezidentské volby v únoru 2013, kdy ekonomicky levicový a kulturně konzervativní Miloš Zeman porazil Karla Schwarzenberga. Relativně těsné vítězství 54,8 ku 45,2 procenta rozdělilo společnost na dvě podobně velké části.

[Povolební šetření agentury CVVM](http://nesstar.soc.cas.cz/webview/) umožňuje analyzovat charakteristiky necelých sedmi stovek respondentů, kteří ve druhém kole prezidentského souboje odvolili. Téměř šedesát procent podpořilo Miloše Zemana. Oproti skutečnému volebnímu výsledku má tedy ve vzorku o přibližně pět procentních bodů víc.

Následující graf prozrazuje, jak by volily jednotlivé sociodemografické skupiny. V kategoriích věku a příjmu jsme respondenty rozdělili na stejně velké poloviny, dělicí čarou je 45 let a příjem 14 tisíc korun čistého. U velikosti sídla jsme využili připravenou škálu a rozdělili respondenty na ty, kteří žijí v obcích s více než 80 tisíci obyvateli, a na ostatní.

<aside class="big">
  <div id="container_prezident" style="height: 600px;"></div>
</aside>

Graf prozrazuje, že od průměrného voliče se svou preferencí nejvíc odlišovali mladí, vysokoškolsky vzdělaní, Pražané a respondenti s vyšším příjmem.

Naopak víra, pohlaví ani umístění na ose město-vesnice při rozhodování nehrály velkou roli. U velikosti sídla to může být překvapivý výsledek. Potvrzuje ho přitom také otázka na subjektivní vnímání obce: Cítíte se být obyvatelem velkého města? Respondenti, kteří odpověděli *ano*, volili téměř stejně jako ti z menších měst a vesnic.

> Zemanovi voliči vykazují podobné charakteristiky jako ti Trumpovi nebo jako voliči brexitu.

„Možná to zní překvapivě, ale v datech totéž pozorujeme už delší dobu,“ komentuje to sociolog Jakub Macek. „Klíč je v tom, že menší sídla mají v Česku velmi různorodý charakter. Jejich volební chování se hodně liší podle regionu, některá jsou navíc předměstí velkých metropolí a volí podobně jako ony. Takže předpovědět jen na základě velikosti obce, kde žijete, zda budete volit toho nebo onoho kandidáta, je prakticky nemožné.“

Odlišně by se tak ve volbách rozhodovala pouze Praha, kde by ve vzorku CVVM zvítězil Karel Schwarzenberg s 53,6 procenta hlasů.

Pro upřesnění, která charakteristika byla při rozhodování pro voliče klíčová, jsme vytvořili statistický model. Ten potvrzuje, že při rozhodování mezi Zemanem a Schwarzenbergem je rozhodující věk, vzdělání a v menší míře také příjem.

Zemanovi voliči tedy vykazují podobné charakteristiky jako ti Trumpovi nebo jako voliči brexitu. Každá volba měla svoje specifika; ve Spojených státech sehrála podstatnou roli rasa, v etnicky homogenním Česku samozřejmě ne. Za oceánem se jinak rozhodovaly ženy a jinak muži, u nás ne. Největší propast je ale ve všech třech zemích podobná: zeje mezi mladšími a bohatšími obyvateli velkých měst s vysokoškolským titulem a mezi staršími, chudšími obyvateli menších měst a vesnic se středoškolským nebo nižším vzděláním.

Vzdělání ovšem rozděluje společnost trochu jiným způsobem, než se na první pohled zdá.

## Kde se berou liberálové – a proč ne na univerzitách <a name="vzdelani"></a>

„Vysokoškolský titul znamenal u Britů vyšší pravděpodobnost, že budou chtít zůstat v unii,“ [shrnuje web Financial Times](https://www.ft.com/content/9fc71e40-b015-11e6-a37c-f4a01f1b0fa1). „Bylo by jednoduché interpretovat to tak, že kvalitní vzdělání vede ke kosmopolitním volebním rozhodnutím. Ale byla by to chyba.“

Jak popisuje zmíněný článek, podle takzvané [kontaktní hypotézy](https://en.wikipedia.org/wiki/Contact_hypothesis) vede setkávání s lidmi z různých sociálních prostředí k vyšší empatii a toleranci jiných světonázorů. Přesně k tomu dochází na univerzitách: řada budoucích studentů opouští domov a v cizím městě se setkává s vrstevníky z nejrůznějších společenských prostředí. Zvlášť v zemi s nejvyšší spotřebou piva na světě – tedy Česku – pak dlouho do noci objevují nové názory. Kontaktní hypotéza vysvětluje, proč by právě univerzity měly vychovávat tolerantní městské liberály.

> Výzkumy naznačují, že vztah vysokoškolského prostředí a liberálního světonázoru je opačný, než se obvykle očekává. Univerzity nevytvářejí třídu městských liberálů.

Ve skutečnosti je pro ni – alespoň podle Financial Times – málo důkazů. [Dlouhodobá studie](http://www.bramlancee.eu/docs/LanceeSarrasinESR2015.pdf), která zkoumala postoje mladých Švýcarů k přistěhovalcům, sice u vysokoškolských studentů skutečně pozitivnější vztah k imigrantům objevila, ten ale nevznikl docházkou na univerzitu. Ti, kteří měli k přistěhovalcům vlídnější postoj, ho vykazovali už v dřívější etapě studie; většina z nich se k nim lépe stavěla už před nástupem na vysokou školu.

Také další výzkumy naznačují, že vztah vysokoškolského prostředí a liberálního světonázoru je opačný, než se obvykle očekává: univerzity nevytvářejí třídu městských liberálů. Ještě nehotoví městští liberálové vyhledávají univerzity, kde si potvrdí svůj pohled na svět. O jejich hodnotovém ukotvení se pravděpodobně rozhoduje mnohem dřív a klíčový vliv v něm mají rodiče, ne vysoká škola.

Toho, že absolventi univerzit volí podstatně jinak než ostatní, si analytici všímají od osmdesátých let minulého století. Důvod, proč se propast mezi oběma tábory projevila až teď, je rostoucí podíl vysokoškolsky vzdělané populace. Ve Spojených státech měl ve čtyřicátých letech vysokoškolský titul každý dvacátý, v osmdesátých letech každý šestý a dnes každý třetí.

<aside class="big">
  <div id="container_vzdelani" style="height: 600px;"></div>
</aside>

Spojené státy i Spojené království patří celosvětově k zemím s nejvyšším podílem absolventů vysokých škol. V Británii se navíc v posledních dvaceti letech číslo téměř zdvojnásobilo. Dnes navštěvuje vysoké školy téměř polovina populace mezi 18 a 30 lety. Vysoká studijní účast může mít nečekané důsledky.

„Přestože protentokrát převážil brexit,“ [píše britský The Guardian](https://www.theguardian.com/politics/2016/oct/05/trump-brexit-education-gap-tearing-politics-apart), „je možné, že za dvacet let budou Britové hlasovat o opětovném připojení k Evropské unii. V tu chvíli už bude vysoký podíl vysokoškolsky vzdělaných i mezi staršími lidmi. Dvacet let je ovšem v politice velmi dlouhá doba a propast mezi více a méně vzdělanými se mezitím může prohloubit.“

Česku zatím nic takového nehrozí. Přestože data o prezidentské volbě ukázala, že vysokoškolské vzdělání bylo při rozhodování o budoucím prezidentovi poměrně zásadní, vysokoškolsky vzdělaných je u nás v porovnání s vyspělými zeměmi málo. Jak ukazuje graf, jejich podíl v české společnosti se sice za posledních dvacet let zdvojnásobil, přesto v této statistice patříme na chvost zemí OECD. Navzdory lepší startovní čáře dnes zaostáváme také třeba za Polskem.

> Otázka trestu smrti dokáže předpovědět volbu pro nebo proti brexitu mnohem přesněji než demografický model.

Sledování propasti mezi lépe a hůře vzdělanými poskytuje alternativní výklad Trumpova vítězství. Demografická data sice umí poměrně jednoduše popsat strukturu obou skupin, možná ale pomíjí klíčové rozdíly v hodnotovém nastavení. To alespoň poměrně přesvědčivě [ukazuje profesor Eric Kaufmann](http://blogs.lse.ac.uk/politicsandpolicy/personal-values-brexit-vote/) z University of London. Ten ukazuje, že jedním z nejpřesnějších volebních prediktorů byl u britského referenda postoj k trestu smrti.

Podle jeho čísel dokázala právě tato otázka předpovědět volbu pro, nebo proti brexitu s mnohem vyšší přesností než model vycházející ze všech demografických charakteristik. Postoj k trestu smrti podle něj dokázal předpovědět individuální hlasování s víc než sedmdesátiprocentní přesností. Všechny demografické charakteristiky dohromady podle Kaufmanna předpověděly výsledek se spolehlivostí něco přes šedesát procent. Trest smrti přitom nikdo ze zastánců brexitu v kampani nezmínil, takže jde o „přirozený“ – a pro statistiky tím cennější – postoj.

„Volba Británie opustit unii je prý protestní hlas těch, kteří doplatili na modernizaci a globalizaci,“ rozkládá Kaufmann. „Londýn proti regionům, chudí proti bohatým. Nic není vzdálenější pravdě. Voliče brexitu, stejně jako příznivce Trumpa, motivují hodnoty, ne ekonomika. Věk, vzdělání, národní identita a etnicita jsou důležitější než příjem nebo zaměstnání. Ale pokud se chceme dostat na dřeň toho, co oba tábory odděluje, musíme se podívat ještě hlouběji, na úroveň osobnosti a postojů.“

<blockquote>Voliče brexitu stejně jako příznivce Trumpa motivují hodnoty, ne ekonomika.<br>
<span class="citautor">Eric Kaufmann, politolog</blockquote>

„Vynořuje se nové politické štěpení: na jedné straně jsou ti, kteří chtějí pořádek, na druhé ti, kteří preferují otevřenost,“ dodává Kaufmann.

Kaufmannova tvrzení se opírají také o mapu postojů britských euroskeptiků. Ta shrnuje výsledky [předloňské studie](http://www.cultdyn.co.uk/articles.html), která na ostrovech sledovala hodnotovou orientaci. Teplejší barvy ukazují skeptický postoj k Evropské unii. Postoje, které se u lidí vyskytovaly zároveň, k sobě mají blízko také v mapě. Výzkumníci v mapě identifikovali tři skupiny: takzvané *osadníky* (nahoře), pro které je důležitá bezpečnost, disciplína a tradiční rodina, *prospektory* orientované na úspěch (vlevo) a individualistické a kulturně zaměřené *pionýry* (vpravo). Euroskeptický postoj se velmi silně překrývá s konzervativními *osadníky*. (Mimochodem: ti se také ztotožňují s tvrzením *Málo produktů vydrží tak dlouho, jak nám tvrdí reklama, to dřív nebývalo*.)


<figure>
    <img src="https://samizdat.cz/data/stepeni-2016/charts/dynamika.png" width="60%" align="center" style="margin-left: 30%">
<figcaption>
<small><i>
Mapa postojů britských euroskeptiků. Teplejší barvy ukazují skeptický postoj k Evropské unii. Postoje, které se u lidí vyskytovaly zároveň, k sobě mají blízko také v mapě. Popisky necháváme v původním jazyce. <a href="http://www.cultdyn.co.uk/articles.html" target="_blank">Zdroj: Cultural Dynamics</a>
</i></small>
</figcaption>
</figure>

## Česko 2017: Čeká nás politická krize? <a name="polarizace"></a>

Nahromaděná data z poslední doby nutně neříkají, že západní společnost prožívá těžší krizi než kdy dříve. Zákopy mezi různými skupinami se neobjevily v okamžiku, kdy si Amerika zvolila prezidentem Donalda Trumpa. Existovaly dávno předtím a teprve prezidentská volba z nich udělala téma.

Hrozí ale akutní politická krize Česku? Vyhrocené politické kampaně a mediální přestřelky poslední doby působí dojmem, že minimálně od pádu komunismu nebyla společnost tak silně rozdělená. Pravidelná šetření Centra pro výzkum veřejného mínění ovšem ukazují něco jiného.

> Největší zásah utrpěla česká důvěra v demokracii po roce 1998 – tedy v okamžiku dohody Václava Klause a Miloše Zemana.

Na jednu stranu data přinášejí špatnou zprávu – u většiny ukazatelů, které vypovídají o polarizaci ve společnosti, její stabilitě nebo spokojenosti s režimem, je hodnocení prachbídné. Vedle ní ale nesou i dobrou zvěst: od roku 2001, kdy jsou k dispozici kompletní data, se situace výrazně nehorší. Naopak, od roku 2012, kdy nepopulární vláda Petra Nečase řešila koaliční krizi a spokojenost s politikou klesla na rekordních pět procent, se většina hodnocení odrazila ode dna. Poměrně dobrou zprávou je i to, že na klíčovou otázku – zda je demokratický režim nejlepším způsobem vlády – odpovídá kladně víc než polovina respondentů.

<aside class="big">
  <div id="container_duvera" style="height: 600px;"></div>
</aside>

Delší perspektiva, kterou v [odborném časopise Acta Politologica](http://acpo.vedeckecasopisy.cz/publicFiles/001208.pdf) nabízí sociolog Lukáš Linek z české Akademie věd, je ovšem skeptičtější. Linek sleduje data o důvěře v demokracii a politické instituce až na začátek devadesátých let.

Podíl těch, kdo jsou přesvědčení o oprávněnosti demokratického režimu, se podle jeho čísel mezi rokem 1990 a 2015 propadl o 25 až 30 procentních bodů. Největší zásah dostala česká demokracie po roce 1998 – tedy v okamžiku, kdy se dva největší političtí oponenti devadesátých let dohodli na překvapivé spolupráci. Václav Klaus, Miloš Zeman a opoziční smlouva znamenaly pro důvěru v demokracii asi desetiprocentní propad.

„Čeští občané sledují politiku a jsou ochotní se angažovat téměř tak silně jako v devadesátých letech,“ vysvětluje Linek. „Ovšem dramaticky se u nich zvedla politická nespokojenost, stejně jako nedůvěra a cynismus k politickým institucím. Tuto změnu doprovodil stále kritičtější přístup k demokratickému režimu.“

> Ti, kteří prožili víc než polovinu života za komunismu, si dokážou život v nedemokratickém režimu představit snadněji.

„Na druhou stranu, bylo by naivní předpokládat, že dnešní úroveň podpory demokracie bude stejná jako na počátku devadesátých let,“ dodává Linek.

Linek také ukazuje, že podpora demokracie je v Česku oproti západu nízká. Zatímco u nás se pohybuje někde pod šedesáti procenty, v Dánsku je to 93 procent, v Norsku 88 procent a na území bývalého západního Německa 84 procent. Česko je v tomto ukazateli na úrovni okolních postkomunistických zemí.

Odpověď na stejnou otázku – tedy otázku po podpoře demokratického režimu – má ovšem pro Česko také jeden nadějnější aspekt. Od západních demokracií nás totiž odlišuje ještě jednou, tentokrát poněkud nečekaným směrem. [Deník The New York Times ukazuje](http://www.nytimes.com/2016/11/29/world/americas/western-liberal-democracy.html), že podpora demokracie v řadě zemí u mladší generace klesá.

*V následujícím grafu si můžete kliknutím na název země zobrazit její čísla.*

<aside class="big">
  <div id="container_duveraveksvet" style="height: 600px;"></div>
</aside>

V Česku se postoje mladých a starších také liší, ale opačně. Stále živá památka komunistického režimu vyvolává u starší generace nostalgii, u mladší spíše odpor. Ti, kteří prožili víc než polovinu života za komunismu, si dokážou život v nedemokratickém režimu představit snadněji.

Dokonale srovnat české a západní postoje k demokracii nemůžeme vzhledem k poněkud odlišným kritériím výzkumníků: na západě sledovali podíl respondentů, pro které je demokracie naprosto nezbytná, u nás se ptají na souhlas s poněkud mírnějším tvrzením, že je demokracie nejlepší způsob vlády. I tak je rozdílný trend mezi Českem a zavedenými demokraciemi podstatný.

<aside class="big">
  <div id="container_duveravek" style="height: 600px;"></div>
</aside>

Data o spokojenosti a důvěře Čechů ovšem ukazují ještě jeden velký problém: rychle klesá důvěra v média.

„Ve výzkumu Reuters Institute se důvěra českých uživatelů internetu v média ukázala být jednou z nejnižších,“ [vysvětluje v rozhovoru pro deník E15](http://nazory.e15.cz/clanek/rozhovory/vaclav-stetka-blizime-se-svetu-bez-profesionalnich-novinaru-1308347) sociolog médií Václav Štětka, který působí na britské Loughborough University. „V západních zemích je stále ještě nadpoloviční. V Česku je to jinak, jen 34 procent uživatelů internetu udává, že věří médiím. Individuálním novinářům ještě méně, jenom dvacet procent.“

Důvodem je podle Štětky ekonomická krize po roce 2009, která zvlášť tvrdě dopadla na středo a východoevropský region. Reklamní trh se v některých zemích regionu propadl během dvou nebo tří let až o polovinu.

„V České republice je situace velmi unikátní v míře, v jaké média vlastní podnikatelské skupiny, jejichž primárním centrem jsou úplně jiné ekonomické sektory,“ pokračuje Štětka. „Média si (podnikatelé, pozn. red.) přikoupili do struktury svých firem, aniž by očekávali, že budou nějakým způsobem vydělávat. To je hlavní rozdíl proti vlastníkům v zahraničí. Tam se ve většině případů vlastníci výhradně nebo primárně pohybují na poli médií.“

<blockquote>Antisystémové myšlení je výrazně přiživováno falešnými zprávami, hoaxy nebo konspiracemi.<br>
<span class="citautor">Lukáš Likavčan, A2larm</blockquote>

Společně s dalšími trendy, jako je rostoucí dominance sociálních sítí a nástup dezinformačních webů v minulém roce, [o kterém jsme psali před měsícem](https://interaktivni.rozhlas.cz/dezinformace/), může nedůvěra v profesionální novináře znamenat rychlou polarizaci společnosti. K čemu nedůvěra v klasická média může vést, ilustrují [vizualizace na webu VICE](https://news.vice.com/story/journalists-and-trump-voters-live-in-separate-online-bubbles-mit-analysis-shows). Ukazují, že průsečík informačních zdrojů, které na Twitteru sledují příznivci Donalda Trumpa a Hillary Clintonové, je prakticky prázdný. Mezi oběma tábory neexistuje ani základní shoda na tom, které informační zdroje jsou důležité. Vizualizace mimochodem ukazují také to, že informační zdroje Trumpových příznivců nesledují téměř žádní profesionální novináři.

Doktorand na Fakultě sociálních studií brněnské Masarykovy univerzity Lukáš Likavčan navíc v [článku pro A2larm upozorňuje](http://a2larm.cz/2016/12/krajina-zmatenych-milenialu/), že zvlášť citelně může nedůvěra v tradiční média zasáhnout mladou generaci. Ukazuje to na voličské základně Ľudové strany Naše Slovensko fašistického vůdce Mariána Kotleby. Jeho (v naprosté většině mladí) voliči jsou na jednu stranu nejaktivnější ve vyhledávání informací, na druhou stranu řada jejich zdrojů publikuje především vymyšlené novinky. „Antisystémové myšlení je výrazně přiživováno falešnými zprávami, hoaxy nebo konspiracemi,“ píše Likavčan.

Odhalit propasti v české společnosti může také to, že zdejší volební systém – na rozdíl od toho amerického i britského – voliče nenutí k volbě buď-anebo. Proporční systém při volbě do Poslanecké sněmovny umožňuje zastoupení řadě názorových alternativ, včetně několika antisystémových. Hlas pro KSČM nebo Úsvit, které uspěly v posledních parlamentních volbách, tak jednak může české společnosti pomáhat upouštět páru, také ale upřesnit, proti čemu protest míří.

<figure>
<img src="media/protest.jpg" width="100%" alt="Síla protestu" title="Síla protestu">
<figcaption>
<small><i>
Síla protestu. Demonstrace iniciativy Islám v České republice nechceme, březen 2015. Ilustrační foto: <a href="https://commons.wikimedia.org/wiki/File:IV%C4%8CRN_14-03-2015_4.JPG">Venca24/Wikimedia Commons (CC BY 4.0)</a>
</i></small>
</figcaption>
</figure>

Populistické strany a témata, proti kterým jejich voliči protestují, sleduje Ondřej Císař z Fakulty sociálních věd Univerzity Karlovy. „Obvykle se předpokládá, že nespokojenost s demokracií vede k podpoře populistů,“ vysvětluje Císař. „Vzhledem k tomu, že je u nás řada populistických politických aktérů, můžeme sledovat různé motivace voličů k jejich podpoře.“

„Voliči komunistické strany zpochybňují legitimitu demokratického režimu, voliči Úsvitu podobu demokracie a voliči Babišova hnutí ANO mají výhrady ke korupci a neschopnosti současných politiků. Preferují manažerský styl vlády.“

Hypotézu obou výzkumníků jsme otestovali na datech ze šetření agentury CVVM za uplynulých patnáct let. U komunistů i voličů Úsvitu se potvrdila beze zbytku. Přesvědčení, že demokratický režim je lepší než autoritářský, je ze všech politických stran nejnižší u voličů KSČM (14 procent), zatímco voliči Úsvitu i ANO mají nadprůměrnou důvěru v demokracii (53 procent, respektive 57 procent). V otázkách na spokojenost s politickou situací, důvěru v Poslaneckou sněmovnu nebo nezkorumpovanost politické elity už si voliči KSČM s voliči Úsvitu notovali, v obou případech se podíl kladných odpovědí pohyboval hluboko pod průměrem.

<blockquote>Voliči KSČM zpochybňují legitimitu demokratického režimu, voliči Úsvitu podobu demokracie a voliči Babišova hnutí ANO mají výhrady ke korupci a neschopnosti současných politiků.<br>
<span class="citautor">Ondřej Císař, sociolog</blockquote>

Voliči ANO se ukazovali spíše jako optimisté s nadstandardní důvěrou v politický systém i Poslaneckou sněmovnu.

„To není nic překvapivého,“ reaguje Císař. „Za prvé, voliči úspěšných stran logicky po volbách vykazují větší spokojenost s politickým systémem i důvěru v Poslaneckou sněmovnu. A za druhé, náš model ukazuje, že pro voliče ANO byla nejsilnějším politickým tématem korupce. V našem volebním modelu dokázal postoj ke korupci velmi dobře předpovědět, zda budete volit ANO, či nikoliv.“

Přijít na základě zmíněných dat s jednoduchým konstatováním, jak hluboké jsou propasti v české společnosti, je obtížné. Krátké shrnutí tématu by ale snad mohlo znít takto: česká společnost nemá k demokracii příliš silný vztah, nicméně v posledních letech se zdá být stabilní a má mechanismy, kterými dokáže snižovat napětí. Nejhlubší propast zeje mezi mladými a starými. Právě tahle neshoda a vztah mladých k demokracii dává však do budoucna naději, že bude líp.

## Dekonsolidující demokracie. Je čas ke znepokojení <a name="svet"></a>

Loni v březnu narazil [americký analytický web Vox](http://www.vox.com/2016/3/1/11127424/trump-authoritarianism) na studii, která předpověděla úspěch Donalda Trumpa dlouho předtím, než se objevil. Američtí vědci Marc Hetherington a Jonathan Weiler vydali v roce 2009 knihu [Autoritarismus a polarizace v americké společnosti](https://www.amazon.com/Authoritarianism-Polarization-American-Politics-Hetherington/dp/052171124X/ref=sr_1_1?ie=UTF8&qid=1456938606&sr=8-1&keywords=hetherington+weiler+authoritarianism). V ní popsali překvapivě velkou skupinu voličů, kteří hledají silného lídra.

Autoritáři podle vědců preferují společenský řád a hierarchii, která do chaotického světa přináší pocit kontroly. Problémy jako rostoucí diverzita společnosti, příliv imigrantů nebo rozpad starého řádu vnímají jako osobní ohrožení.

„Pracující střední třída se od nedávné recese ocitla pod mimořádným ekonomickým tlakem,“ píše Vox. „Bílým navíc hrozí ztráta privilegované pozice, kterou si dosud mohli být jistí. Vzhledem k migraci jim hrozí, že se z nich v několika dalších desetiletích stane menšina. Prezident je černoch a v populární kultuře jsou stále častější jiná než bílá etnika.“

> Trump není příčinou americké krize, ale jejím důsledkem.

Podle autorů článku tato skupina tíhne k republikánům, kteří od šedesátých let razí konzervativní politiku. Zároveň hledají tvrdší řešení, než strana nabízí. Proto vytvářejí stranu ve straně a hrozí, že republikány svými hlasy roztrhnou na dvě politické strany. V posledních volbách se jejich favoritem stal právě Trump.

„Od ostatních kandidátů Trumpa odlišuje rétorika a styl,“ doplňuje Vox. „Způsob, kterým cokoliv omezí na černobílý spor mezi dvěma extrémy.“

Podle autorů studie není Trump příčinou americké krize, ale jejím důsledkem. Skupina autoritářských voličů zůstane v americké politice přítomná nezávisle na současném prezidentovi. V černé prognóze autoři slibují, že společenské rozdíly, které je aktivují, porostou. A přestože vědci mluví o Spojených státech, stejná skupina voličů, kteří hledají silné lídry, pravděpodobně existuje všude na světě.

[Deník The New York Times](http://www.nytimes.com/2016/11/29/world/americas/western-liberal-democracy.html) se dívá i mimo Spojené státy a sleduje ohrožení demokracie ve velké části vyspělého světa. Zmiňuje teorii politické konsolidace, podle které dlouho platilo, že jakmile některá země dosáhne určitého bohatství, založí demokratické instituce a rozvine občanskou společnost, je vyhráno. Teorie, která se spolehlivě potvrzovala několik desetiletí, nyní přestává fungovat. [Index svobody](https://freedomhouse.org/), který sleduje společnost Freedom House, začal v roce 2005 globálně klesat.

Americko-australská dvojice vědců, která sleduje demokratické trendy, proto vyvinula jednoduchý test. Slouží jako systém včasného varování a skládá se ze tří částí. Za prvé se ptají, jak důležité je pro občany země, aby zůstala demokratická. Za druhé sledují jejich otevřenost nedemokratickým formám vlády, jako je vojenská diktatura. A za třetí hledají, jakou podporu mají protisystémové strany — tedy politici, kteří tvrdí, že současný systém je nelegitimní, v Česku typicky komunisté. Pokud se všechny tři faktory posunují špatným směrem, označují výzkumníci zemi za opak konsolidující se země, tedy *dekonsolidující demokracii*.

> Podobné trendy jako v Polsku jsou dnes podle výzkumníků patrné v řadě západních demokracií včetně Spojených států. A týkají se i Česka.

Ve středoevropském regionu je zvlášť zajímavým příkladem jejich teorie Polsko. Když země v roce 2004 vstoupila do Evropské unie, byla podle The New York Times všeobecně považována za postkomunistickou zemi s mimořádně dobře zvládnutým přechodem k demokracii. Oba výzkumníci ale už tehdy pozorovali silné známky dekonsolidace: 16 procent Poláků nedůvěřovalo demokracii, 22 procent by bývalo podpořilo vojenskou diktaturu, od roku 2000 se v parlamentu objevovaly protisystémové strany. Po roce 2015, kdy se vlády ujala strana Právo a spravedlnost a pustila se do systematického oslabování demokratických institucí, se jejich pochyby o úspěšné přeměně potvrzují.

Podobné trendy jsou dnes podle dvojice vědců patrné v řadě západních demokracií včetně Spojených států. A týkají se i Česka.

The New York Times upozorňuje, že apokalyptické vyznění výzkumu má svoje rezervy: jednak závisí na datech ze šetření veřejného mínění, která nemusejí být stoprocentně přesná, jednak nebere v úvahu další faktory podstatné pro stabilitu společnosti, jako je ekonomický růst. Upozorňuje ovšem také na [text, který s teorií dekonsolidace polemizuje](http://www.journalofdemocracy.org/article/danger-deconsolidation-how-much-should-we-worry).

„Je to jen jedna metrika,“ připouštějí sami výzkumníci. „Jenže,“ dodávají jedním dechem, „měla by nás znepokojit.“